﻿/* File to store all gulp configuration  */

module.exports = function () {
    var args = require('yargs').argv;
    var root = __dirname + '/';
    var assetRoot = root + 'Static/';

    var customCssRoot = assetRoot + 'Source/css';
    var handlebarsRoot = assetRoot + 'templates/';

    var config = {
        isBuild: !!(args.build),
        scriptPath: assetRoot + 'js/',
        stylePath: assetRoot + 'css/',
        fontPath: assetRoot + 'fonts/',
        templatePath: assetRoot + 'templates/',
        templatePartialPath: assetRoot + 'templates/partials',
        templateOutputPath: assetRoot + 'templates/dist',

        styles: [
            customCssRoot + '/**/*.scss',
            customCssRoot + '/**/*.css'
        ],
        libs: [

        ],
        templates: [
            handlebarsRoot + '**/*.hbs'
        ]
    }

    return config;
};