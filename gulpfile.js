﻿/*
    Gulp workflow
*/

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var runSeq = require('run-sequence');
var del = require('del');
var config = require('./gulp.config')();
var handlebars = require('handlebars');
var gulpHandlebars = require('gulp-handlebars-html')(handlebars);
var rename = require('gulp-rename');
var regexRename = require('gulp-regex-rename');
var replace = require('gulp-replace');

// Global error handler
function errorHandler(error) {
    console.log('beep: ', beep);
    plugins.util.log(error);
};


gulp.task('default',['processStyles'], function () {
   
});

gulp.task('clean-styles', function (done) {
    return del([config.stylePath], done);
});

gulp.task('compileHtml', function () {
    var templateData = {
    },
    options = {
        partialsDirectory: [config.templatePartialPath]
    };

    return gulp.src(config.templatePath + "*.page.hbs")
        .pipe(gulpHandlebars(templateData, options))
        .pipe(regexRename(/\.page\.hbs$/, ".html"))
        .pipe(replace(/\uFEFF/gi, "")) //cut out ZERO WIDTH nbsp characters the compiler adds in
        .pipe(gulp.dest(config.templateOutputPath));

});

gulp.task('compileHtml:watch', function () {
    return gulp.watch(config.templates, ['compileHtml']);
});

gulp.task('sass', function () {
    return gulp.src(config.styles)
    .pipe(plugins.sass.sync())
    .pipe(gulp.dest(config.stylePath))
    .on('error', errorHandler);
});


gulp.task('compressedStyles', function () {
    return gulp.src(config.styles)
    .pipe(plugins.rename({ extname: '.min.css' }))
    .pipe(plugins.sass.sync({ outputStyle: 'compressed' }))
    .pipe(gulp.dest(config.stylePath))
    .on('error', errorHandler);
});

gulp.task('stylesWithoutClean', function () {
    runSeq('sass', 'compressedStyles');
});

gulp.task('styles',function () {
    runSeq('clean-styles', 'sass', 'compressedStyles');
});

gulp.task('libs', function () {
    for (var i in config.libs) {
        var lib = config.libs[i];
        (function(l) {
            gulp.src(l.input)
            .pipe(gulp.dest(l.output))
            .on('error', errorHandler);
        })(lib);
    }
    return;
});

gulp.task('styles:watch', function () {
    return gulp.watch(config.styles, ['styles']);
});

gulp.task('stylesWithoutClean:watch', function () {
    return gulp.watch(config.styles, ['stylesWithoutClean']);
});